import argparse
import os

import mlflow
from sklearn import datasets
from sklearn.linear_model import LinearRegression

from iris_wrapper import IrisWrapper

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://65.21.60.234:19001"
os.environ["MLFLOW_TRACKING_URL"] = "http://65.21.60.234:5000"
os.environ["AWS_ACCESS_KEY_ID"] = "IAM_ACCESS_KEY"
os.environ["AWS_SECRET_ACCESS_KEY"] = "IAM_SECRET_KEY"

mlflow.set_tracking_uri("http://65.21.60.234:5000")


def update_model(threshold_0_1: float, threshold_1_2: float):
    iris = datasets.load_iris()
    X = iris["data"][:, :2]
    y = iris["target"]

    client = mlflow.tracking.MlflowClient()

    experiment = client.get_experiment_by_name("iris-wrapper")
    print(experiment)

    with mlflow.start_run(
        experiment_id=experiment.experiment_id, run_name="iris_wrapper"
    ) as run:
        lr = LinearRegression().fit(X, y)
        wrapper = IrisWrapper(lr, threshold_0_1, threshold_1_2)
        mlflow.pyfunc.log_model(
            "model",
            python_model=wrapper,
            code_path=["iris_wrapper.py"],
            registered_model_name="iris_wrapper",
        )

    version = next(
        filter(
            lambda x: x.current_stage == "None",
            client.get_registered_model("iris_wrapper").latest_versions,
        )
    ).version

    print(f"version: {version}")

    client.transition_model_version_stage(
        name="iris_wrapper",
        version=version,
        stage="Staging",
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("threshold_0_1", type=float)
    parser.add_argument("threshold_1_2", type=float)
    args = parser.parse_args()
    update_model(args.threshold_0_1, args.threshold_1_2)
